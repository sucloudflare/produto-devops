#!/bin/bash

# Crie um Projeto CodeBuild
aws codebuild create-project \
    --name meu-projeto-codebuild \
    --source "sourceType=NO_SOURCE" \
    --artifacts "type=S3,location=meu-bucket-s3" \
    --environment "type=LINUX_CONTAINER,image=aws/codebuild/standard:4.0"
