  <h1>Integração do GitLab com AWS CodeBuild e CodePipeline</h1>
  <img src='a.webp' alt=''>
 <div class="section">
  <h2>Pré-requisitos:</h2>
        <ul>
            <li>Conta no AWS: Certifique-se de que você tem uma conta ativa na AWS.</li>
            <li>Repositório no GitLab: Tenha um repositório GitLab que você deseja integrar com o AWS CodeBuild e CodePipeline.</li>
        </ul>
    </div>
<div class="section">
 <h2>Passos para Configuração:</h2>

 <h3>1. Criar um Token de Acesso Pessoal no GitLab</h3>
        <ol>
            <li>Vá para as configurações do seu perfil no GitLab.</li>
            <li>Selecione <strong>"Tokens de acesso"</strong>.</li>     <li>Crie um novo token com permissões de leitura do repositório (<code>read_repository</code>).</li>
            <li>Anote o token gerado, pois ele será usado posteriormente.</li>
        </ol>

<h3>2. Configurar um Projeto do CodeBuild</h3>
        <ol>
            <li>Acesse o Console de Gerenciamento da AWS e vá para o CodeBuild.</li>
            <li>Clique em <strong>"Criar projeto de construção"</strong>.</li>
            <li>Preencha os detalhes do projeto, como nome e descrição.</li>
            <li>Na seção <strong>"Source"</strong>, selecione <strong>"GitHub"</strong> como provedor de origem.</li>
            <li>Cole o URL do repositório GitLab.</li>
            <li>Escolha <strong>"OAuth"</strong> como método de autenticação e forneça o Token de Acesso Pessoal criado anteriormente.</li>
            <li>Configure os detalhes do ambiente, especificando a imagem Docker e o tipo de ambiente.</li>
            <li>Configurar o <code>buildspec.yml</code> ou adicione os comandos diretamente na interface.</li>
            <li>Revise e crie o projeto.</li>
        </ol>

 <h3>3. Configurar um Pipeline no CodePipeline</h3>
<ol>
            <li>Acesse o Console de Gerenciamento da AWS e vá para o CodePipeline.</li>
            <li>Clique em <strong>"Criar pipeline"</strong>.</li>
            <li>Dê um nome ao pipeline e selecione um novo role de serviço ou use um existente.</li>
            <li>Adicione uma Stage de origem:
                <ul>
                    <li>Selecione <strong>"GitHub"</strong> como provedor de origem.</li>
                    <li>Autentique com o GitHub usando o Token de Acesso Pessoal do GitLab.</li>
                    <li>Especifique o repositório e a branch que deseja usar.</li>
                </ul>
            </li>
            <li>Adicione uma Stage de build:
                <ul>
                    <li>Selecione <strong>"AWS CodeBuild"</strong> como provedor de build.</li>
                    <li>Escolha o projeto de build criado anteriormente.</li>
                </ul>
            </li>
            <li>Configure estágios adicionais como implantação, se necessário.</li>
            <li>Revise e crie o pipeline.</li>
        </ol>
    </div>

 <div class="section">
        <h3>4. Configurar Webhook no GitLab</h3>
        <p>Para que as alterações no repositório GitLab acionem automaticamente o pipeline:</p>
        <ol>
            <li>No GitLab, vá para <strong>Settings > Webhooks</strong> do projeto.</li>
            <li>Adicione um novo webhook com a URL fornecida pelo AWS CodePipeline:
                <pre><code>https://pipelines.aws-region.amazonaws.com/v1/webhooks?projectName=nome-do-pipeline&region=aws-region&secret=token-secreto</code></pre>
            </li>
            <li>Selecione os eventos que deseja acionar o webhook (por exemplo, Push events, Merge requests).</li>
            <li>Salve o webhook.</li>
        </ol>
    </div>

<div class="section">
        <h3>Exemplo de Arquivo <code>buildspec.yml</code></h3>
        <pre><code>version: 0.2

phases:
  install:
    commands:
      - echo Installing dependencies...
      - apt-get install -y jq
  pre_build:
    commands:
      - echo Pre-build phase...
      - echo Logging in to Amazon ECR...
      - $(aws ecr get-login --no-include-email --region us-east-1)
  build:
    commands:
      - echo Build started on `date`
      - echo Building the Docker image...
      - docker build -t my-image .
      - docker tag my-image:latest 123456789012.dkr.ecr.us-east-1.amazonaws.com/my-image:latest
  post_build:
    commands:
      - echo Pushing the Docker image...
      - docker push 123456789012.dkr.ecr.us-east-1.amazonaws.com/my-image:latest
artifacts:
  files:
    - '**/*'</code></pre>
    </div>

 <div class="section summary">
        <h2>Resumo</h2>
        <ul>
            <li>Crie um Token de Acesso Pessoal no GitLab.</li>
            <li>Configure um projeto no CodeBuild com o repositório GitLab.</li>
            <li>Configure um pipeline no CodePipeline usando o projeto do CodeBuild.</li>
            <li>Configure um webhook no GitLab para acionar o pipeline automaticamente.</li>
        </ul>
        <p>Seguindo esses passos, você conseguirá integrar seu repositório GitLab ao AWS CodeBuild e CodePipeline, permitindo automação de build e deploy contínuos.</p>
    </div>

<h1>Hospedagem de um Site de Portfólio em React na AWS</h1>

<div class="section">
        <h2>Passo a Passo para Hospedagem</h2>

 <h3>1. Desenvolvimento e Build do Projeto React</h3>
        <ol>
            <li>Crie seu projeto React e faça o build usando <code>npm run build</code>.</li>
        </ol>

  <h3>2. Configuração do S3</h3>
        <ol>
            <li>Crie um bucket no S3 e habilite a hospedagem estática.</li>
            <li>Faça upload dos arquivos do build (diretório <code>build/</code>) para o bucket.</li>
            <li>Configure permissões de leitura pública para os arquivos.</li>
        </ol>

  <h3>3. Configuração do CloudFront</h3>
        <ol>
            <li>Crie uma distribuição CloudFront.</li>
            <li>Defina o bucket S3 como origem.</li>
            <li>Configure regras de cache e SSL (opcional).</li>
        </ol>

  <h3>4. Registro de Domínio e DNS</h3>
        <ol>
            <li>Registre um domínio no Route 53.</li>
            <li>Configure registros A/AAAA para apontar para o CloudFront.</li>
        </ol>

  <h3>5. Automação de Deploy</h3>
        <ol>
            <li>Configure um pipeline no CodePipeline.</li>
            <li>Integre com CodeBuild para fazer build do projeto React.</li>
            <li>Adicione etapas de deploy para enviar os arquivos para o S3.</li>
        </ol>

 <h3>6. Segurança</h3>
        <ol>
            <li>Configure o AWS WAF e Shield para proteger seu site.</li>
            <li>Adicione regras de segurança no WAF para bloquear tráfego malicioso.</li>
        </ol>

 <h3>7. Monitoramento e Logs</h3>
        <ol>
            <li>Configure CloudWatch para monitorar métricas do S3, CloudFront e Lambda (se usado).</li>
            <li>Use CloudWatch Logs para coletar logs de acesso e erros.</li>
        </ol>
    </div>

 <h1>Implementação de Microserviços</h1>

 <div class="section">
        <h2>Amazon ECS ou EKS</h2>

 <h3>Amazon ECS (Elastic Container Service)</h3>
        <ol>
            <li>Crie um cluster ECS.</li>
            <li>Defina tarefas e serviços para seus contêineres.</li>
        </ol>

  <h3>Amazon EKS (Elastic Kubernetes Service)</h3>
        <ol>
            <li>Crie um cluster EKS.</li>
            <li>Implemente seus serviços como pods no Kubernetes.</li>
        </ol>
    </div>

<div class="section">
        <h2>AWS Lambda</h2>
        <p>Use AWS Lambda para executar código em resposta a eventos.</p>
        <ol>
            <li>Configure funções Lambda para tarefas específicas que não requerem um servidor dedicado.</li>
        </ol>
    </div>

<div class="section">
        <h2>Automação</h2>

 <h3>AWS CodePipeline</h3>
        <ol>
            <li>Configure pipelines CI/CD para automatizar builds e deploys.</li>
            <li>Use CodePipeline com CodeBuild para compilar e testar seu código.</li>
            <li>Integre com CodeDeploy para implantar mudanças automaticamente.</li>
        </ol>

 <h3>AWS CloudFormation</h3>
        <p>Use CloudFormation para definir e provisionar recursos AWS usando código.</p>
        <ol>
            <li>Crie templates CloudFormation para gerenciar infraestrutura como código (IaC).</li>
        </ol>
    </div>

<div class="section">
        <h2>Logs e Monitoramento</h2>

  <h3>Amazon CloudWatch</h3>
        <ol>
            <li>Monitore e colete logs das suas aplicações e serviços.</li>
            <li>Configure métricas e alarmes para monitorar performance e saúde.</li>
            <li>Use CloudWatch Logs para coletar e visualizar logs de aplicações.</li>
        </ol>

  <h3>Raio X da AWS</h3>
        <p>Rastreamento de requisições para identificar problemas de performance.</p>
        <ol>
            <li>Integre X-Ray com seus serviços para analisar latência e identificar gargalos.</li>
        </ol>
    </div>

<div class="section">
        <h2>Segurança e Firewall</h2>

 <h3>AWS WAF (firewall de aplicativos da Web)</h3>
        <p>Proteja seu site contra ataques comuns da web (SQL injection, XSS, etc.).</p>
        <ol>
            <li>Configure regras WAF para filtrar tráfego indesejado.</li>
        </ol>

 <h3>Escudo AWS</h3>
        <p>Proteção contra ataques DDoS.</p>
        <ol>
            <li>Utilize AWS Shield Standard (gratuito) ou Advanced (pago) para proteção adicional.</li>
        </ol>
    </div>

 <div class="section">
        <h2>Calculadora de Preços da AWS</h2>

 <h3>Calculadora de preços AWS</h3>
        <p>Use a calculadora de preços da AWS para estimar custos.</p>
        <ol>
            <li>Insira os serviços e recursos que você planeja usar para obter uma estimativa de custo mensal.</li>
        </ol>
    </div>

<div class="section">
        <h2>Escalabilidade e Disponibilidade</h2>
   <h3>Dimensionamento automático</h3>
        <p>Configure auto scaling para ajustar automaticamente a capacidade dos seus serviços com base na demanda.</p>
        <ol>
            <li>Configure políticas de escalabilidade para EC2, ECS ou EKS.</li>
        </ol>

 <h3>Amazon RDS ou DynamoDB</h3>
        <p>Use Amazon RDS para bancos de dados relacionais com alta disponibilidade.</p>
        <ol>
            <li>Configure instâncias multi-AZ para tolerância a falhas.</li>
        </ol>
        <p>Use DynamoDB para bancos de dados NoSQL altamente escaláveis.</p>
    </div>

<div class="section summary">
        <h2>Considerações Finais</h2>
        <p>Com essa configuração, o site de portfólio em React estará hospedado na AWS, utilizando uma arquitetura de microserviços com alta disponibilidade, escalabilidade automática, segurança robusta e monitoramento completo. Utilizei a calculadora de preços da AWS para estimar os custos baseados na utilização dos serviços mencionados. Isso garantirá que seu site funcione de maneira eficiente e segura, independentemente da demanda.</p>
    </div>